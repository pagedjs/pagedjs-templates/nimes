---
author: Kiara Jouhanneau | @kajou.design | kajou.design@gmail.com | ko-fi.com/kajoudesign    
title: Nîmes  
project: paged.js sprint
book format: letter
book orientation: portrait
version: v1.0
---
  
## → fonts/type designers :  
### Blatant by [Tomás Castiglioni](https://www.behance.net/gallery/97006355/Blatant-Free-font)  
>*Blatant is a versatile font made to draw the attention, it won't let your creation go unnoticed. It stands out through its set of alternate wide capitals.*  
### Lora by [Cyreal](https://fonts.google.com/specimen/Lora)  
>*Lora is a well-balanced contemporary serif with roots in calligraphy. It is a text typeface with moderate contrast well suited for body text.  
Technically Lora is optimised for screen appearance, and works equally well in print.*
### IA Writer Mono by [Information Architects Inc.](https://github.com/iaolo/iA-Fonts)  
>*iA Writer comes bundled with iA Writer for Mac and iOS.This is a modification of IBM's Plex font. The upstream project is [here](https://github.com/IBM/plex)*
    

## → the following classes :  
>> **Component Basis**  
component-body  
component-front  
component-title  
content-opener-image  
decoration  
paragraph  

>> **Component: Long**  
biography  
case-study  
feature   
long  

>> **Component: Shortboxes**  
note  
reminder  
short  
tip  
warning  

>> **Features: Closing**  
key-term  
key-terms  
references  
review-activity  
summary  

>> **Page: Chapter**  
chapter  
chapter-number  
focus-questions  
introduction  
learning-objectives  
outline  
outline-remake  

>> **Page: Part**  
part  
part-number  
outline  


>> **Page: TOC**  
ct  
toc   
toc-body  
toc-chapter  
toc-part  
name  

>> **Others**    
restart-numbering  
running-left  
running-right  
start-right  

